package com.zarbosoft.atstdlib;

import com.zarbosoft.luxem.write.Writer;

import java.io.OutputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.zarbosoft.rendaw.common.Common.uncheck;

public class GenerateClass {
  public final Path path;
  public final Class klass;

  public GenerateClass(Class klass) {
    this.path = outPath(klass);
    this.klass = klass;
    Main.sync.push(this);
  }

  private Path outPath(Class klass) {
    return uncheck(
        () -> {
          String[] parts = klass.getCanonicalName().split("\\.");
          Path base = Paths.get("out");
          for (int i = 0; i < parts.length - 1; ++i) {
            String part = parts[i];
            base = base.resolve(part);
          }
          return base.resolve(parts[parts.length - 1] + ".at");
        });
  }

  public LuxemValue lRecord(List<ROPair<LuxemValue, LuxemValue>> values) {
    return writer -> {
      writer.type("record").recordBegin().key("elements").arrayBegin();
      for (ROPair<LuxemValue, LuxemValue> value : values) {
        writer.type("record-element").recordBegin().key("key");
        value.first.write(writer);
        writer.key("value");
        value.second.write(writer);
        writer.recordEnd();
      }
      writer.arrayEnd().recordEnd();
    };
  }

  public LuxemValue lRecord(ROPair<LuxemValue, LuxemValue>... values) {
    return lRecord(Arrays.asList(values));
  }

  public LuxemValue lTuple(List<LuxemValue> values) {
    return writer -> {
      writer.type("tuple").recordBegin().key("values").arrayBegin();
      for (LuxemValue value : values) {
        value.write(writer);
      }
      writer.arrayEnd().recordEnd();
    };
  }

  public LuxemValue lString(String text) {
    return writer -> {
      writer.type("string").recordBegin().key("value").primitive(text).recordEnd();
    };
  }

  public LuxemValue lCall(LuxemValue f, LuxemValue args) {
    return writer -> {
      writer.type("call").recordBegin().key("target");
      f.write(writer);
      writer.key("arg");
      args.write(writer);
      writer.recordEnd();
    };
  }

  public LuxemValue lAccess(LuxemValue target, LuxemValue key) {
    return writer -> {
      writer.type("access").recordBegin().key("parent");
      target.write(writer);
      writer.key("child");
      key.write(writer);
      writer.recordEnd();
    };
  }

  public LuxemValue lBuiltin() {
    return writer -> {
      writer.type("builtin").recordBegin().recordEnd();
    };
  }

  public LuxemValue lLocal(LuxemValue key) {
    return lAccess(
        writer -> {
          writer.type("local").recordBegin().recordEnd();
        },
        key);
  }

  public LuxemValue lBind(LuxemValue name, LuxemValue value) {
    return writer -> {
      writer.type("bind").recordBegin().key("name");
      name.write(writer);
      writer.key("value");
      value.write(writer);
      writer.recordEnd();
    };
  }

  public LuxemValue lType(Class klass) {
    if (klass == this.klass) {
      return lLocal(lString("type"));
    } else if (klass.isArray()) {
      return lCall(
          lAccess(lLocal(lString("java")), lString("array")), lType(klass.getComponentType()));
    } else if (klass == void.class) {
      return lAccess(lBuiltin(), lString("void"));
    } else if (klass == int.class
        || klass == short.class
        || klass == long.class
        || klass == byte.class
        || klass == float.class
        || klass == double.class
        || klass == boolean.class
        || klass == char.class) {
      return lAccess(lLocal(lString("java")), lString(klass.getName()));
    } else {
      return lCall(
          lAccess(lBuiltin(), lString("module_local")),
          lString(path.relativize(new GenerateClass(klass).path).toString()));
    }
  }

  public LuxemValue lType(Parameter[] params) {
    List<LuxemValue> values = new ArrayList<>();
    for (Parameter param : params) {
      values.add(lType(param.getType()));
    }
    return lTuple(values);
  }

  public LuxemValue lReturn(LuxemValue value) {
    return writer -> {
      writer.type("return").recordBegin().key("value");
      value.write(writer);
      writer.recordEnd();
    };
  }

  public void generate() {
    System.out.format("Generating %s\n", path);
    uncheck(
        () -> {
          Files.createDirectories(path.getParent());
          try (OutputStream os = Files.newOutputStream(path)) {
            Writer writer = new Writer(os, (byte) '\t', 1);
            writer.type("alligatorus:0.0.1").arrayBegin();
            lBind(lString("java"), lAccess(lBuiltin(), lString("java"))).write(writer);

            lBind(
                    lString("type"),
                    lCall(
                        lAccess(lLocal(lString("java")), lString("class")),
                        lString(klass.getCanonicalName())))
                .write(writer);

            List<ROPair<LuxemValue, LuxemValue>> classPairs = new ArrayList<>();

            if (klass.getSuperclass() != null) {
              classPairs.add(new ROPair<>(lString("extends"), lType(klass.getSuperclass())));
            }

            Set<String> usedNames = new HashSet<>();
            {
              Map<String, List<LuxemValue>> methodSignatures = new HashMap<>();
              for (Method m : klass.getDeclaredMethods()) {
                methodSignatures
                    .computeIfAbsent(m.getName(), _m -> new ArrayList<>())
                    .add(
                        lCall(
                            lAccess(lLocal(lString("java")), lString("signature")),
                            lRecord(
                                new ROPair<>(lString("arguments"), lType(m.getParameters())),
                                new ROPair<>(lString("return"), lType(m.getReturnType())))));
              }
              List<ROPair<LuxemValue, LuxemValue>> methods = new ArrayList<>();
              for (Method m : klass.getDeclaredMethods()) {
                usedNames.add(m.getName());
                methods.add(
                    new ROPair<>(
                        lString(m.getName()),
                        lRecord(
                            new ROPair<>(lString("extern_name"), lString(m.getName())),
                            new ROPair<>(
                                lString("types"), lTuple(methodSignatures.get(m.getName()))))));
              }
              classPairs.add(new ROPair<>(lString("methods"), lRecord(methods)));
            }

            {
              List<ROPair<LuxemValue, LuxemValue>> fields = new ArrayList<>();
              for (Field f : klass.getDeclaredFields()) {
                String name = f.getName();
                while (usedNames.contains(name)) {
                  name = name + "_";
                }
                fields.add(
                    new ROPair<>(
                        lString(name),
                        lRecord(
                            new ROPair<>(lString("extern_name"), lString(f.getName())),
                            new ROPair<>(lString("type"), lType(f.getType())))));
              }
              classPairs.add(new ROPair<>(lString("fields"), lRecord(fields)));
            }

            lCall(lAccess(lLocal(lString("type")), lString("define_extern")), lRecord(classPairs))
                .write(writer);

            List<ROPair<LuxemValue, LuxemValue>> out = new ArrayList<>();
            out.add(new ROPair<>(lString("type"), lLocal(lString("type"))));

            List<LuxemValue> constructorSignatures = new ArrayList<>();
            for (Constructor constructor : klass.getConstructors()) {
              constructorSignatures.add(
                  lCall(
                      lAccess(lLocal(lString("java")), lString("signature")),
                      lRecord(
                          new ROPair<>(lString("arguments"), lType(constructor.getParameters())),
                          new ROPair<>(lString("return"), lLocal(lString("type"))))));
            }

            lReturn(lRecord(out)).write(writer);
            writer.arrayEnd();
          }
        });
  }

  private interface LuxemValue {
    void write(Writer writer);
  }

  public static class ROPair<T, U> {
    public final T first;
    public final U second;

    public ROPair(T first, U second) {
      this.first = first;
      this.second = second;
    }
  }
}
